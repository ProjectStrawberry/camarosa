package com.gmail.jd4656.camarosa;

import com.destroystokyo.paper.event.server.AsyncTabCompleteEvent;
import com.palmergames.bukkit.towny.event.MobRemovalEvent;
import com.palmergames.bukkit.towny.event.TownUpkeepCalculationEvent;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityToggleGlideEvent;
import org.bukkit.event.entity.VillagerAcquireTradeEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class EventListeners implements Listener {

    Main plugin;
    WorldGuardPlugin worldGuard;
    RegionContainer container;

    EventListeners(Main p) {
        plugin = p;
        worldGuard = (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin("WorldGuard");
        container = WorldGuard.getInstance().getPlatform().getRegionContainer();
    }

    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEntityEvent event) {
        Entity clicked = event.getRightClicked();
        Player player = event.getPlayer();
        if (clicked instanceof ItemFrame && player.isSneaking()) {
            ItemFrame itemFrame = (ItemFrame) clicked;
            ItemStack item = itemFrame.getItem();
            if (item.getType() == Material.AIR) return;
            ItemMeta meta = item.getItemMeta();

            event.setCancelled(true);

            if (meta.getDisplayName().length() > 0 || meta.hasLore()) player.sendMessage(ChatColor.DARK_PURPLE + "[" + ChatColor.LIGHT_PURPLE + "Item Frame Information" + ChatColor.DARK_PURPLE + "]");
            if (meta.getDisplayName().length() > 0) player.sendMessage(meta.getDisplayName());

            if (meta.hasLore()) {
                for (String lore : meta.getLore()) {
                    player.sendMessage(lore);
                }
            }
        }
    }

    @EventHandler
    public void townyMobDespawn(MobRemovalEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof LivingEntity) {
            EntityEquipment equipment = ((LivingEntity) entity).getEquipment();
            if (equipment == null) return;

            if (equipment.getItemInMainHand().getType() != Material.AIR) {
                entity.getWorld().dropItem(entity.getLocation(), equipment.getItemInMainHand());
            }

            if (equipment.getItemInOffHand().getType() != Material.AIR) {
                entity.getWorld().dropItem(entity.getLocation(), equipment.getItemInOffHand());
            }

            for (ItemStack item : equipment.getArmorContents()) {
                if (item.getType() == Material.AIR) continue;
                entity.getWorld().dropItem(entity.getLocation(), item);
            }
        }
    }

    @EventHandler
    public void elytraGlide(EntityToggleGlideEvent event) {
        Entity entity = event.getEntity();

        if (entity instanceof Player) {
            Player player = (Player) entity;
            LocalPlayer localPlayer = worldGuard.wrapPlayer(player);
            RegionQuery query = container.createQuery();
            ApplicableRegionSet set = query.getApplicableRegions(localPlayer.getLocation());

            if (!set.testState(localPlayer, Main.ELYTRA_USE)) {
                ItemStack elytra = player.getInventory().getChestplate();
                if (elytra != null && elytra.getType() == Material.ELYTRA) {
                    entity.sendMessage(ChatColor.RED + "Elytra's are disabled in this region");
                    ItemStack placeholder = new ItemStack(Material.LEATHER_CHESTPLATE);
                    ItemMeta placeholderMeta = placeholder.getItemMeta();
                    assert placeholderMeta != null;

                    placeholderMeta.addEnchant(Enchantment.BINDING_CURSE,1, true);
                    placeholderMeta.addEnchant(Enchantment.VANISHING_CURSE,1, true);
                    placeholderMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    placeholderMeta.setDisplayName(ChatColor.RED + "Elytra's are currently disabled in this area");

                    placeholder.setItemMeta(placeholderMeta);
                    player.getInventory().setChestplate(placeholder);
                    player.updateInventory();

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            player.getInventory().setChestplate(elytra);
                            player.updateInventory();
                        }
                    }.runTaskLater(plugin, 20L);
                }
            }
        }
    }

    @EventHandler
    public void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        String message = event.getMessage();
        if (message.equals("/t spawn Greendale")) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + event.getPlayer().getName() + " bot");
        }

        if (plugin.showChannel.contains(event.getPlayer().getUniqueId())) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    plugin.sendActionBarChannel(event.getPlayer());
                }
            }.runTaskLater(plugin, 1);
        }
    }

    @EventHandler
    public void CreatureSpawnEvent(CreatureSpawnEvent event) {
        if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.VILLAGE_DEFENSE) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void TownUpkeepCalculationEvent(TownUpkeepCalculationEvent event) {
        int multiplier = 50;

        Player player = Bukkit.getPlayer(event.getTown().getMayor().getName());
        if (player != null) {
            multiplier = plugin.getUpkeep(player);
            if (!plugin.townyCache.containsKey(player.getName())) {
                plugin.townyCache.put(player.getName(), multiplier);
                plugin.saveTowny();
            }
        } else {
            multiplier = plugin.townyCache.getOrDefault(event.getTown().getMayor().getName(), multiplier);
        }

        event.setUpkeep(event.getTown().getNumResidents() * multiplier);
    }

    @EventHandler
    public void inventoryClickEvent(InventoryClickEvent event) {
        if (event.getInventory().getType() == InventoryType.SHULKER_BOX) {
            ItemStack item = event.getCurrentItem();
            if (item != null && item.getAmount() > item.getMaxStackSize()) {
                event.setCancelled(true);
                event.getWhoClicked().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "WARNING: " + ChatColor.DARK_RED + "Items stacked using /stack will disappear if placed into shulker boxes.");
            }
        }

        if (event.getInventory().getType() == InventoryType.MERCHANT) {
            if (event.getClickedInventory() == null || event.getClickedInventory().getType() != InventoryType.MERCHANT || event.getSlotType() != InventoryType.SlotType.RESULT) return;

            InventoryAction action = event.getAction();
            MerchantInventory inv = (MerchantInventory) event.getInventory();
            MerchantRecipe recipe = inv.getSelectedRecipe();

            Entity entity = (Entity) event.getInventory().getHolder();

            if (recipe != null) {
                ItemStack item = recipe.getResult();
                if (item.hasItemMeta() && item.getItemMeta() instanceof EnchantmentStorageMeta && ((EnchantmentStorageMeta) item.getItemMeta()).hasStoredEnchant(Enchantment.MENDING)) {
                    recipe.setMaxUses(2);

                    assert entity != null;
                    int villagerUses = plugin.villagers.getOrDefault(entity.getUniqueId(), 0);

                    if (villagerUses >= 2) {
                        event.setCancelled(true);
                        return;
                    }

                    if (action == InventoryAction.PICKUP_ALL) {
                        villagerUses++;
                        plugin.villagers.put(entity.getUniqueId(), villagerUses);
                        plugin.saveVillagers();
                    }

                    if (action == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
                        villagerUses += (recipe.getMaxUses() - villagerUses);
                        plugin.villagers.put(entity.getUniqueId(), villagerUses);
                        plugin.saveVillagers();
                    }
                }
            }
        }
    }

    @EventHandler
    public void VillagerAcquireTradeEvent(VillagerAcquireTradeEvent event) {
        MerchantRecipe recipe = event.getRecipe();
        ItemStack item = recipe.getResult();
        if (item.hasItemMeta()) {
            if (item.getItemMeta() instanceof EnchantmentStorageMeta && ((EnchantmentStorageMeta) item.getItemMeta()).hasStoredEnchant(Enchantment.MENDING)) {
                recipe.setMaxUses(2);
                event.setRecipe(recipe);
            }
        }
    }
}
