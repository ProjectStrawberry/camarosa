package com.gmail.jd4656.camarosa;

import mineverse.Aust1n46.chat.api.MineverseChatAPI;
import mineverse.Aust1n46.chat.channel.ChatChannel;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;



public class ShowChannel implements CommandExecutor {

    Main plugin;
    ShowChannel(Main p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }

        Player player = (Player) sender;

        if (plugin.showChannel.contains(player.getUniqueId())) {
            plugin.showChannel.remove(player.getUniqueId());
        } else {
            plugin.showChannel.add(player.getUniqueId());

            plugin.sendActionBarChannel(player);
        }

        return true;
    }
}
