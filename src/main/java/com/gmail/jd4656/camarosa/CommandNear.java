package com.gmail.jd4656.camarosa;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

import java.util.ArrayList;
import java.util.List;

public class CommandNear implements TabExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "This command does not work from the console.");
            return true;
        }
        if (!sender.hasPermission("camarosa.near")) {
            sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        int radius = 16;

        if (sender.hasPermission("camarosa.near.apprentice")) radius = 50;
        if (sender.hasPermission("camarosa.near.prodigy")) radius = 100;
        if (sender.hasPermission("camarosa.near.educated")) radius = 150;
        if (sender.hasPermission("camarosa.near.professional")) radius = 200;

        if (args.length > 0) {
            try {
                int argRadius = Integer.parseInt(args[0]);
                if (argRadius < radius || sender.hasPermission("camarosa.near.nolimit")) radius = argRadius;
            } catch (NumberFormatException ignored) {}
        }

        Player player = (Player) sender;

        List<String> nearbyPlayers = new ArrayList<>();

        Location playerLoc = player.getLocation();

        for (Player curPlayer : Bukkit.getOnlinePlayers()) {
            if (curPlayer.getUniqueId().equals(player.getUniqueId())) continue;
            if (!curPlayer.getWorld().getName().equals(playerLoc.getWorld().getName())) continue;
            if (isVanished(curPlayer) && !player.hasPermission("camarosa.near.seevanished")) continue;
            int distance = getDistance(playerLoc, curPlayer.getLocation());
            if (distance <= radius) {
                nearbyPlayers.add(ChatColor.RESET + curPlayer.getDisplayName() + ChatColor.WHITE + "(" + ChatColor.DARK_RED + distance + "m" + ChatColor.WHITE + ")");
            }
        }

        if (nearbyPlayers.isEmpty()) {
            player.sendMessage(ChatColor.GOLD + "No players nearby");
            return true;
        }

        player.sendMessage(ChatColor.GOLD + "Players nearby: " + String.join(", ", nearbyPlayers));
        return true;
    }

    int getDistance(Location loc, Location targetLoc) {
        return (int) Math.floor(Math.sqrt(loc.distanceSquared(targetLoc)));
    }

    private boolean isVanished(Player player) {
        for (MetadataValue meta : player.getMetadata("vanished")) {
            if (meta.asBoolean()) return true;
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        return null;
    }
}
