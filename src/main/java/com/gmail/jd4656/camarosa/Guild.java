package com.gmail.jd4656.camarosa;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class Guild {
    private String id;
    private String desc;
    private String color;
    private String name;

    private Material material;
    
    Map<String, GuildRank> ranks = new HashMap<>();

    Guild(String id) {
        this.id = id;
        this.name = id.substring(0, 1).toUpperCase() + id.substring(1, id.length());
    }

    public GuildRank getRank(String rankId) {
        return this.ranks.get(rankId);
    }

    public String getColor() {
        return this.color;
    }

    public String getName() {
        return this.name;
    }

    public ItemStack getItem(Player player) {
        ItemStack item = new ItemStack(this.material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(this.color + ChatColor.BOLD + this.name);

        if (this.material == Material.POTION) itemMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);

        List<String> lore = new ArrayList<>();

        if (player != null) {
            if (player.hasPermission("camarosa.classes." + this.id)) {
                lore.add(this.color + "You're a member of this class.");
            } else if (!player.hasPermission("camarosa.classes.nojoin")) {
                lore.add(this.color + "Click to become a member of this class.");
            }
        }
        lore.add(this.color + this.desc);

        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);

        return item;
    }

    public void setColor(String color) {
        this.color = ChatColor.translateAlternateColorCodes('&', color);
    }

    public void setItemMaterial(String material) {
        this.material = Material.getMaterial(material);
    }

    public void addRank(String rankId, GuildRank rank) {
        ranks.put(rankId, rank);
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }
}
