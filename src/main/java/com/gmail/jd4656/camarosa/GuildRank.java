package com.gmail.jd4656.camarosa;

public class GuildRank {

    private String id;
    private String name;

    private int acrobatics, herbalism, excavation, woodcutting,
            archery, swords, taming, alchemy, repair, mining,
            fishing, axes;
    private int money;
    private int rewardMoney = 0;
    private int points = 0;

    private boolean extraClass = false;

    GuildRank(String id) {
        this.id = id;
        this.name = id.substring(0, 1).toUpperCase() + id.substring(1, id.length());
    }

    public int getRewardMoney() {
        return this.rewardMoney;
    }

    public boolean getExtraClass() {
        return this.extraClass;
    }

    public String getName() {
        return this.name;
    }

    public int getPoints() {
        return this.points;
    }

    public void setExtraClass(boolean option) {
        this.extraClass = option;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setPoints(int amount) {
        this.points += amount;
    }

    public void setRewardMoney(int amount) {
        this.rewardMoney += amount;
    }
/*
    public void setStats(PrimarySkillType skill, int level) {
        switch (skill) {
            case ACROBATICS:
                this.acrobatics = level;
                break;
            case HERBALISM:
                this.herbalism = level;
                break;
            case EXCAVATION:
                this.excavation = level;
                break;
            case WOODCUTTING:
                this.woodcutting = level;
                break;
            case ARCHERY:
                this.archery = level;
                break;
            case SWORDS:
                this.swords = level;
                break;
            case TAMING:
                this.taming = level;
                break;
            case ALCHEMY:
                this.alchemy = level;
                break;
            case REPAIR:
                this.repair = level;
                break;
            case MINING:
                this.mining = level;
                break;
            case FISHING:
                this.fishing = level;
                break;
            case AXES:
                this.axes = level;
                break;
        }
    }*/
}
