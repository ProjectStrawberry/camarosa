package com.gmail.jd4656.camarosa;

import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.context.Context;
import net.luckperms.api.model.user.User;
import net.luckperms.api.track.PromotionResult;
import net.luckperms.api.track.TrackManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommandClasses implements TabExecutor {

    Main plugin;
    String[] classNames = {"smith", "artist", "architect", "hunter", "farmer", "aristocrat", "alchemist", "merchant"};

    CommandClasses(Main p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("camarosa.classes")) {
            sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        if (args.length > 0) {
            if (args[0].equals("leave")) {
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GOLD + "/class leave <class>");
                    return true;
                }
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "This command does not work from the console.");
                    return true;
                }

                String className = args[1].toLowerCase();

                boolean found = false;
                for (String cName : classNames) {
                    if (cName.equals(className)) found = true;
                }

                if (!found) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "That class does not exist.");
                    return true;
                }

                Player player = (Player) sender;

                if (!player.hasPermission("camarosa.classes." + className)) {
                    sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "You have not joined that class.");
                    return true;
                }

                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " permission unset camarosa.classes.nojoin");
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " parent cleartrack " + className);

                player.sendMessage(ChatColor.GOLD + "You have left that class.");
                return true;
            }
            if (args[0].equals("promote")) {
                if (!sender.hasPermission("camarosa.classes.promote")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (args.length < 3) {
                    sender.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GOLD + "/class promote <player> <class>");
                    return true;
                }

                String playerName = args[1];
                String guildId = args[2].toLowerCase();

                Guild guild = plugin.guilds.get(guildId);

                if (guild == null) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Class not found.");
                    return true;
                }

                Player targetPlayer = Bukkit.getPlayer(playerName);
                if (targetPlayer == null) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                    return true;
                }

                if (!targetPlayer.isOnline()) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player is not online.");
                    return true;
                }

                LuckPerms api = LuckPermsProvider.get();
                api.getContextManager().getContextSetFactory().immutableEmpty();

                TrackManager trackManager = api.getTrackManager();

                User targetUser = api.getUserManager().getUser(targetPlayer.getUniqueId());
                PromotionResult promotion = api.getTrackManager().getTrack(guildId).promote(targetUser, api.getContextManager().getContextSetFactory().immutableBuilder().build());

                Optional<String> groupFrom = promotion.getGroupFrom();
                Optional<String> groupTo = promotion.getGroupTo();

                if (!promotion.getStatus().wasSuccessful()) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Unable to promote user.");
                    return true;
                }

                api.getUserManager().saveUser(targetUser);

                /*if (!groupFrom.isEmpty()) {
                    GuildRank rankFrom = guild.getRank(groupFrom.get());
                }*/

                if (!groupTo.isEmpty()) {
                    GuildRank rankTo = guild.getRank(groupTo.get().replace(guildId, ""));
                    if (rankTo.getRewardMoney() > 0) {
                        Main.getEconomy().depositPlayer(targetPlayer, rankTo.getRewardMoney());
                    }
                    if (rankTo.getPoints() > 0) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "points give " + targetPlayer.getName() + " " + rankTo.getPoints());
                    }
                    if (rankTo.getExtraClass()) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + targetPlayer.getName() + " permission unset camarosa.classes.nojoin");
                    }
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "[" + ChatColor.RED + ChatColor.BOLD + "Camarosa" +  ChatColor.DARK_RED + ChatColor.BOLD + "] " + ChatColor.RESET + plugin.getRankupMessage(rankTo, guild, targetPlayer));
                    targetPlayer.sendMessage(ChatColor.RED + "You've been promoted to " + guild.getColor() + rankTo.getName() + ChatColor.RED + " in the " + guild.getColor() + guild.getName() + ChatColor.RED + " class.");
                    sender.sendMessage(ChatColor.RED + "You've promoted " + guild.getColor() + targetPlayer.getDisplayName() + ChatColor.RESET + ChatColor.RED + " to " + guild.getColor() + rankTo.getName() + ChatColor.RED + " in the " + guild.getColor() + guild.getName() + ChatColor.RED + " class.");
                }

                return true;
            }
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "This command does not work from the console.");
            return true;
        }

        Player player = (Player) sender;

        InventoryManager manager = new InventoryManager(ChatColor.GOLD + ChatColor.UNDERLINE.toString() + "Classes", 9, plugin);

        int invPos = 0;
        for (Guild guild : plugin.guilds.values()) {
            manager.withItem(invPos, guild.getItem(player));
            invPos++;
        }


        manager.withEventHandler(new InventoryClickHandler() {
             @Override
             public void handle(InventoryClickEvent event) {
                 event.setCancelled(true);
                 if (player.hasPermission("camarosa.classes.nojoin")) return;
                 Player player = (Player) event.getWhoClicked();
                 ItemStack clickedItem = event.getCurrentItem();
                 if (clickedItem.getType().equals(Material.AIR)) return;
                 ItemMeta clickedMeta = clickedItem.getItemMeta();
                 String className = ChatColor.stripColor(clickedMeta.getDisplayName());

                 if (player.hasPermission("camarosa.classes." + className.toLowerCase())) return;

                 Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " promote " + className.toLowerCase());
                 Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " permission set camarosa.classes.nojoin");

                 player.sendMessage(ChatColor.GOLD + "You've joined the " + clickedMeta.getDisplayName() + ChatColor.GOLD + " class.");
                 player.closeInventory();
             }
        });

        manager.show(player);

        return true;
    }

    private ItemStack setName(ItemStack item, String name) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return item;
    }


    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) {
            tabComplete.add("leave");
            if (sender.hasPermission("camarosa.classes.promote")) tabComplete.add("promote");
        }
        if (args.length == 2 && args[0].equals("leave")) {
            tabComplete.addAll(plugin.guilds.keySet());
        }
        if (args.length == 2 && args[0].equals("promote") && sender.hasPermission("camarosa.classes.promote")) {
            return null;
        }
        if (args.length == 3 && args[0].equals("promote") && sender.hasPermission("camarosa.classes.promote")) {
            tabComplete.addAll(plugin.guilds.keySet());
        }
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
