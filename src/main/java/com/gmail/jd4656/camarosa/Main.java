package com.gmail.jd4656.camarosa;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import mineverse.Aust1n46.chat.api.MineverseChatAPI;
import mineverse.Aust1n46.chat.api.MineverseChatPlayer;
import mineverse.Aust1n46.chat.channel.ChatChannel;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Main extends JavaPlugin {

    FileConfiguration config;
    Main plugin;

    private static Economy econ = null;
    private Permission perms;

    Map<String, Guild> guilds = new HashMap<>();
    Map<UUID, Integer> villagers = new HashMap<>();
    Map<String, Integer> townyCache = new HashMap<>();


    List<UUID> showChannel = new ArrayList<>();
    List<String> noFly = new ArrayList<>();

    FileConfiguration noFlyList;
    FileConfiguration villagersConfig;
    FileConfiguration townyConfig;

    File noFlyListFile;
    File villagersFile;
    File townyFile;

    public static StateFlag ELYTRA_USE;

    @Override
    public void onLoad() {
        FlagRegistry registry = WorldGuard.getInstance().getFlagRegistry();
        try {
            StateFlag flag = new StateFlag("elytra-use", true);
            registry.register(flag);
            ELYTRA_USE = flag; // only set our field if there was no error
        } catch (FlagConflictException e) {
            Flag<?> existing = registry.get("elytra-use");
            if (existing instanceof StateFlag) {
                ELYTRA_USE = (StateFlag) existing;
            }
        }
    }

    @Override
    public void onEnable() {
        plugin = this;

        plugin.getDataFolder().mkdirs();
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        this.config = plugin.getConfig();

        noFlyListFile = new File(this.getDataFolder() + "/noflylist.yml");
        noFlyList = YamlConfiguration.loadConfiguration(noFlyListFile);
        villagersFile = new File(this.getDataFolder() + "/villagers.yml");
        villagersConfig = YamlConfiguration.loadConfiguration(villagersFile);
        townyFile = new File(this.getDataFolder() + "/towny.yml");
        townyConfig = YamlConfiguration.loadConfiguration(townyFile);

        if (noFlyList.get("worlds") != null) noFly.addAll(noFlyList.getStringList("worlds"));

        if (villagersConfig.get("villagers") != null) {
            for (String key : villagersConfig.getConfigurationSection("villagers").getKeys(false)) {
                villagers.put(UUID.fromString(key), villagersConfig.getInt("villagers." + key));
            }
        }

        if (townyConfig.get("mayors") != null) {
            for (String key : townyConfig.getConfigurationSection("mayors").getKeys(false)) {
                townyCache.put(key, townyConfig.getInt("mayors." + key));
            }
        }

        if (!setupEconomy()) {
            plugin.getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        this.setupPermissions();

        //MineverseChatAPI.getMineverseChatPlayer("").getCurrentChannel().

        new BukkitRunnable() {
            @Override
            public void run() {
                for (UUID uuid : showChannel) {
                    Player player = Bukkit.getPlayer(uuid);
                    if (player == null || !player.isOnline()) continue;

                   sendActionBarChannel(player);
                }
            }
        }.runTaskTimer(this, 0, 40);

        for (String guildId : config.getConfigurationSection("classes").getKeys(false)) {
            Guild curGuild = new Guild(guildId);

            curGuild.setDesc(config.getString("class-settings." + guildId + ".desc"));
            curGuild.setColor(config.getString("class-settings." + guildId + ".color"));
            curGuild.setItemMaterial(config.getString("class-settings." + guildId + ".item"));


            for (String rankId : config.getConfigurationSection("classes." + guildId).getKeys(false)) {
                GuildRank curRank = new GuildRank(rankId);

                /*curRank.setStats(PrimarySkillType.ACROBATICS, config.getInt("classes." + guildId + "." + rankId + ".acrobatics", -1));
                curRank.setStats(PrimarySkillType.HERBALISM, config.getInt("classes." + guildId + "." + rankId + ".herbalism", -1));
                curRank.setStats(PrimarySkillType.EXCAVATION, config.getInt("classes." + guildId + "." + rankId + ".excavation", -1));
                curRank.setStats(PrimarySkillType.WOODCUTTING, config.getInt("classes." + guildId + "." + rankId + ".woodcutting", -1));
                curRank.setStats(PrimarySkillType.ARCHERY, config.getInt("classes." + guildId + "." + rankId + ".archery", -1));
                curRank.setStats(PrimarySkillType.SWORDS, config.getInt("classes." + guildId + "." + rankId + ".swords", -1));
                curRank.setStats(PrimarySkillType.TAMING, config.getInt("classes." + guildId + "." + rankId + ".taming", -1));
                curRank.setStats(PrimarySkillType.ALCHEMY, config.getInt("classes." + guildId + "." + rankId + ".alchemy", -1));
                curRank.setStats(PrimarySkillType.REPAIR, config.getInt("classes." + guildId + "." + rankId + ".repair", -1));
                curRank.setStats(PrimarySkillType.MINING, config.getInt("classes." + guildId + "." + rankId + ".mining", -1));
                curRank.setStats(PrimarySkillType.FISHING, config.getInt("classes." + guildId + "." + rankId + ".fishing", -1));
                curRank.setStats(PrimarySkillType.AXES, config.getInt("classes." + guildId + "." + rankId + ".axes", -1));*/

                curRank.setMoney(config.getInt("classes." + guildId + "." + rankId + ".wealth", -1));
                curRank.setRewardMoney(config.getInt("global-rewards." + rankId + ".money", 0));

                curRank.setPoints(config.getInt("global-rewards." + rankId + ".points", 0));
                curRank.setPoints(config.getInt("classes." + guildId + "." + rankId + ".points", 0));

                curRank.setExtraClass(config.getBoolean("global-rewards." + rankId + ".extraclass", false));

                curGuild.addRank(rankId, curRank);
            }

            guilds.put(guildId, curGuild);
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("classes").setExecutor(new CommandClasses(this));
        this.getCommand("near").setExecutor(new CommandNear());
        this.getCommand("toggleelytra").setExecutor(new CommandToggleElytra(this));
        this.getCommand("bt").setExecutor(new CommandBT());
        this.getCommand("staffmode").setExecutor(new CommandStaffMode());
        this.getCommand("showchannel").setExecutor(new ShowChannel(this));
    }

    void sendActionBarChannel(Player player) {
        MineverseChatPlayer chatPlayer = MineverseChatAPI.getMineverseChatPlayer(player);
        ChatChannel channel = chatPlayer.getCurrentChannel();

        TextComponent component = new TextComponent();
        TextComponent text = new TextComponent(channel.getName());
        TextComponent startBracket = new TextComponent("[");
        TextComponent endBracket = new TextComponent("]");

        startBracket.setColor(ChatColor.of(channel.getChatColorRaw()));
        endBracket.setColor(ChatColor.of(channel.getChatColorRaw()));
        text.setColor(ChatColor.of(channel.getChatColorRaw()));

        if (chatPlayer.hasConversation()) {
            Player targetPlayer = Bukkit.getPlayer(chatPlayer.getConversation());
            if (targetPlayer != null) {
                text.setText("Messaging " + targetPlayer.getDisplayName());
                text.setColor(ChatColor.LIGHT_PURPLE);
                startBracket.setColor(ChatColor.DARK_PURPLE);
                endBracket.setColor(ChatColor.DARK_PURPLE);
            }
        }

        component.addExtra(startBracket);
        component.addExtra(text);
        component.addExtra(endBracket);

        player.sendMessage(ChatMessageType.ACTION_BAR, component);
    }

    int getUpkeep(Player player) {
        if (player.hasPermission("camarosa.classes.aristocrat.recruit")) return 45;
        if (player.hasPermission("camarosa.classes.aristocrat.apprentice")) return 43;
        if (player.hasPermission("camarosa.classes.aristocrat.prodigy")) return 40;

        return 50;
    }

    void saveTowny() {
        for (Map.Entry<String, Integer> entry : townyCache.entrySet()) {
            townyConfig.set("mayors." + entry.getKey(), entry.getValue());
        }
        try {
            townyConfig.save(townyFile);
        } catch (IOException e) {
            plugin.getLogger().warning("Error saving towny file:");
            e.printStackTrace();
        }
    }

    void saveVillagers() {
        for (Map.Entry<UUID, Integer> entry : villagers.entrySet()) {
            villagersConfig.set("villagers." + entry.getKey().toString(), entry.getValue());
        }
        try {
            villagersConfig.save(villagersFile);
        } catch (IOException e) {
            plugin.getLogger().warning("Error saving villagers file:");
            e.printStackTrace();
        }
    }

    void saveElytra() {
        noFlyList.set("worlds", noFly);
        try {
            noFlyList.save(noFlyListFile);
        } catch (IOException e) {
            plugin.getLogger().warning("Error saving no fly list:");
            e.printStackTrace();
        }
    }

    public String getRankupMessage(GuildRank rank, Guild guild, Player player) {
        String message = plugin.config.getString("rankup-message");
        message = message.replace("%user%", player.getDisplayName());
        message = message.replace("%rank%", guild.getColor() + rank.getName());
        message = message.replace("%class%", guild.getColor() + guild.getName());

        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public String getJoinMessage(Guild guild, Player player) {
        String message = plugin.config.getString("rankup-message");
        message = message.replace("%user%", player.getDisplayName());
        message = message.replace("%class%", guild.getColor() + guild.getName());

        return ChatColor.translateAlternateColorCodes('&', message);
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Economy getEconomy() {
        return econ;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }

    public Permission getPermissions() {
        return perms;
    }
}
