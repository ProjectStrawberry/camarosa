package com.gmail.jd4656.camarosa;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandToggleElytra implements CommandExecutor {

    Main plugin;

    CommandToggleElytra(Main p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("camarosa.toggleelytra")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");;
            return true;
        }

        Player player = (Player) sender;
        String worldName = player.getWorld().getName();

        if (plugin.noFly.contains(worldName)) {
            plugin.noFly.remove(worldName);
            player.sendMessage(ChatColor.GREEN + "Elytras have been enabled in this world.");
        } else {
            plugin.noFly.add(worldName);
            player.sendMessage(ChatColor.RED + "Elytras have been disabled in this world.");
        }

        plugin.saveElytra();

        return true;
    }
}
