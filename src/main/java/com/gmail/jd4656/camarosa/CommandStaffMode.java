package com.gmail.jd4656.camarosa;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class CommandStaffMode implements CommandExecutor {
    public CommandStaffMode() {
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        } else if (!sender.hasPermission("camarosa.staffmode")) {
            sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "You do not have permission to use this command.");
            return true;
        } else {
            Player player = (Player)sender;
            ConsoleCommandSender var10000;
            String var10001;
            LuckPerms api;
            String group;
            if (!sender.hasPermission("camarosa.staffmode.active")) {
                api = LuckPermsProvider.get();
                group = api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup();
                var10000 = Bukkit.getConsoleSender();
                var10001 = player.getName();
                Bukkit.dispatchCommand(var10000, "lp user " + var10001 + " parent add " + group + "active");
                sender.sendMessage(ChatColor.GREEN + "You are now in staff mode.");
                return true;
            } else {
                api = LuckPermsProvider.get();
                group = api.getUserManager().getUser(player.getUniqueId()).getPrimaryGroup();
                var10000 = Bukkit.getConsoleSender();
                var10001 = player.getName();
                Bukkit.dispatchCommand(var10000, "lp user " + var10001 + " parent remove " + group);
                sender.sendMessage(ChatColor.RED + "You are no longer in staff mode.");
                return true;
            }
        }
    }
}