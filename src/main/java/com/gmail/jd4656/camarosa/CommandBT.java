package com.gmail.jd4656.camarosa;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class CommandBT implements TabExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("camarosa.buildlead")) {
            sender.sendMessage(ChatColor.DARK_RED + "Error: " + ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        if (args.length >= 2 && args[0].equals("promote")) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + args[1] + " parent add builder");
            sender.sendMessage(ChatColor.GREEN + "That player has been promoted to the build team.");
            return true;
        }

        if (args.length >= 2 && args[0].equals("demote")) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + args[1] + " parent remove builder");
            sender.sendMessage(ChatColor.GREEN + "That player has been removed from the build team.");
            return true;
        }

        sender.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/bt promote/demote <player>");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1 && sender.hasPermission("camarosa.buildlead")) {
            tabComplete.add("promote");
            tabComplete.add("demote");
        }
        if (args.length == 2 && sender.hasPermission("camarosa.buildlead")) {
            tabComplete.add("<player>");
        }
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
